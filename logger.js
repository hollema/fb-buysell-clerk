const winston = require('winston');
const level = process.env.LOG_LEVEL || 'debug';
const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
            level: level
        })
    ]
});

module.exports = logger;
