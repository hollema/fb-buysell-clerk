var express = require('express');
var app = express();
var fbGraphService = require('../../services/fbGraphService');
var randomstring = require('randomstring');
var WordPOS = require('wordpos'), wordpos = new WordPOS();

const crypto = require('crypto');

var port = process.env.PORT || 3001;
app.set('port', port);
app.use(express.static(__dirname + '/public'));

/*******************************************************************************/
var delay = 10000; // ms
var numberPosts = 10;

//var groupId = '918676468308249'; // BST
var groupId = '224466948085853'; // Yakkity yak
//var groupId = '816535228506367'; // totes kray sales BST
//var groupId = '175434193017200'; // hideaway
//var groupId = '1674986239240820'; // phew poo
//var groupId = '275050039667029'; // overhead underwear BST

/*******************************************************************************/

var client_id = process.env.CLIENT_ID;
var redirect_uri = process.env.REDIRECT_URI.replace(3000, port);
var token = '';
var appsecret_proof = '';

app.get('/', function(req, res) {
    res.send('<a type="button" href="/login">Log in</a>');
});

app.get('/login', function(req, res) {
    var path = "https://www.facebook.com/v2.10/dialog/oauth";
    var url = `${path}?client_id=${client_id}&display=popup&redirect_uri=${redirect_uri}`;
    res.redirect(url);
});

app.get('/login/return', function(req, res) {
    var code = req.query.code;
    exchangeToken(code, res, postToFB);
});

app.get('/seed', function(req, res) {
    postToFB(res);
    res.send('Finished seeding.');
});

app.listen(app.get('port'), function() {
    console.log("Node app is running at localhost:" + app.get('port'))
});

function exchangeToken(code, response, callback) {
    var path = "https://graph.facebook.com/v2.10/oauth/access_token";
    var client_secret = process.env.CLIENT_SECRET;
    var url = `${path}?client_id=${client_id}&redirect_uri=${redirect_uri}&client_secret=${client_secret}&code=${code}`;
    require('request')(url, function(error, res, body) {
        if (error) {
            console.log(error);
            response.send(error);
        }
        token = JSON.parse(body).access_token;
        console.log('token: ' + token);
        appsecret_proof = generateHmac(token, process.env.CLIENT_SECRET);
        response.send('<a type="button" href="/seed">Seed posts</a>');
    });
}

async function postToFB(res) {
    for (var i=0; i < numberPosts; i++) {
        let message = await buildMessage();
        fbGraphService.publishPost(groupId, message, '', token, appsecret_proof).then(function(result) { console.log(result);
        }).catch(function(err) {
            console.log(err);
        });
        await sleep(delay);
    }
}

function postBatchToFB(res) {
    var messages = [];

    for (var i=0; i < numberPosts; i++) {
        messages.push(buildMessage());
    }

    fbGraphService.publishGroupPosts(groupId, messages, token, appsecret_proof).then(function(result) {
        console.log(result);
        res.send(result);
    }).catch(function(err) {
        console.log(err);
        res.send(err);
    });
}

function sleep(ms) {
    return new Promise(resolve=>{
        setTimeout(resolve,ms);
    });
}

function generateHmac (data, secretKey) {
    var hmac = crypto.createHmac('sha256', secretKey).update(data).digest('hex');
    return hmac;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function buildMessage() {
    var price = getRandomInt(1,99);
    //var description = randomstring.generate();

    return new Promise((resolve, reject) => { 
        wordpos.randNoun(noun => { 
            var message = noun + " - $" + price; // sale post
            //var message = noun; // discussion post
            resolve(message);
        })
    });
}
