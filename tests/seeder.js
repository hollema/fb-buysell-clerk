var seeder = {};

var webdriver = require('selenium-webdriver');
var by = webdriver.By;
var until = webdriver.until;
var randomstring = require('randomstring');
require('dotenv').config({ path: './tests/.env.tests' })

//var phantomjs = require('selenium-webdriver/phantomjs');

var driver; 

seeder.seed = function() {
    var chromeCapabilities = webdriver.Capabilities.chrome();
    var chromeOptions = {
        'args': ['--disable-notifications', '--window-size=600x400'] //, '--headless']
    };
    chromeCapabilities.set('chromeOptions', chromeOptions);
    driver = new webdriver.Builder().withCapabilities(chromeCapabilities).build(); 

    //driver = new phantomjs.Driver();

    driver.get('http://www.facebook.com');
    login();

    var groupName = process.env.GROUP;
    goToGroup(groupName);

    var numberItems = process.env.NUMBER_ITEMS;
    for (var i=0; i<numberItems; i++) {
        postSaleItem();
        driver.manage().timeouts().implicitlyWait(10000); //does nothing
    }

    driver.quit();
};

function login() {
    var email = process.env.FB_USER;
    var pass = process.env.FB_PASS;
    driver.findElement(by.name('email')).sendKeys(email);
    driver.findElement(by.name('pass')).sendKeys(pass);

    var loginButtonLocator = by.css('#loginbutton input');
    driver.wait(until.elementLocated(loginButtonLocator), 200000);
    driver.findElement(loginButtonLocator).click();
}

function goToGroup(groupName) {
    // find group via left nav shortcut
    var shortcuts = driver.findElement(by.css(`span[data-testid="bookmark_icon_left_nav"]`));
    var groupShortcutLocator = `//*[text()='${groupName}']`;
    shortcuts.findElement(by.xpath(groupShortcutLocator)).click();
}

function postSaleItem() {
    // title
    var itemTitle = randomstring.generate();
    var itemTitleEl = driver.findElement(by.css('input[type="text"][placeholder="What are you selling?"]'));
    itemTitleEl.sendKeys(itemTitle);
    itemTitleEl.click();

    // price
    var price = getRandomInt(1,999);
    var addPriceLocator = by.css('input[type="text"][placeholder="Add price"]');
    driver.wait(until.elementLocated(addPriceLocator), 2000);
    var addPriceEl = driver.findElement(addPriceLocator);
    addPriceEl.sendKeys(price);

    // zip code
    var zip = getRandomInt(1,99999);
    var zipCodeLocator = by.css('input[type="text"][placeholder="Zip Code"]');
    driver.wait(until.elementLocated(zipCodeLocator), 2000);
    var zipCodeEl = driver.findElement(zipCodeLocator);
    /*
    for (var i=0, j = zip.length; i<j; i++) {
        console.log(zip.toString().charAt(i));
        zipCodeEl.sendKeys(zip.toString().charAt(i));
    }
    */
    zipCodeEl.sendKeys(zip);

    var postItemModalLocator = by.css('div[role="dialog"]');
    var postItemModalEl = driver.findElement(postItemModalLocator);

    // description
    var description = randomstring.generate();
    var itemDescriptionLocator = by.css('div[data-testid="status-attachment-mentions-input"][role="textbox"]');
    driver.findElement(itemDescriptionLocator).sendKeys(description);

    driver.findElement(by.css('button[data-testid="react-composer-post-button"]')).click(); // post
    driver.wait(until.stalenessOf(addPriceEl), 1000000);
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}


module.exports = seeder;
