var express = require('express');
var router = express.Router();
var SaleController = require('../controllers/saleController');
var saleController = new SaleController();

router.get('/:groupid', function(req, res, next) {
    saleController.getGroup(req, res, next);
});

router.get('/:groupid/posts', function(req, res, next) {
    saleController.getSalePosts(req, res, next);
});

router.get('/:groupid/reload-posts', function(req, res, next) {
    saleController.reloadPosts(req, res, next);
});

router.get('/:groupid/discussion-posts', function(req, res, next) {
    saleController.getDiscussionPosts(req, res, next);
});

router.post('/:groupid/posts/deleteAll', function(req, res, next) {
    saleController.deleteAllPosts(req, res, next);
});

router.post('/:groupid/posts/delete', function(req, res, next) {
    saleController.deleteSalePosts(req, res, next);
});

module.exports = router;
