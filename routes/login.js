const logger = require('../logger');

var express = require('express');
var router = express.Router();
var passport = require('passport');

const PERMISSIONS = ['groups_access_member_info', 'publish_to_groups'];

router.get('/', passport.authenticate('facebook', { 
    scope: PERMISSIONS, 
    callbackURL: process.env.REDIRECT_URI,
}), function(req, res, next) {
    logger.error("Passport facebook authentication failed.");
    next(err);
});

// Login callback
router.get('/return', passport.authenticate('facebook', { 
    successRedirect: '/dashboard', 
    failureRedirect: '/',
    callbackURL: process.env.REDIRECT_URI,
}), function(req, res, next) {
    logger.error("Passport facebook authentication failed in /return.");
    next(err);
});

module.exports = router;
