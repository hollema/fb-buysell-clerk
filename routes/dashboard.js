var express = require('express');
var router = express.Router();
var DashboardController = require('../controllers/dashboardController');
var dashboardController = new DashboardController();

router.get('/', function(req, res, next) {
    dashboardController.getGroupsForUser(req, res, next);
});

module.exports = router;
