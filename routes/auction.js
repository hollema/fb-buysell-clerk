var express = require('express');
var router = express.Router();
var AuctionController = require('../controllers/auctionController');
var auctionController = new AuctionController();

router.get('/:groupid/posts', function(req, res, next) {
    auctionController.getAuctionPosts(req, res, next);
});

router.get('/:groupid/posts/previous', function(req, res, next) {
    auctionController.getPreviousPageAuctionPosts(req, res, next);
});

router.get('/:groupid/posts/next', function(req, res, next) {
    auctionController.getNextPageAuctionPosts(req, res, next);
});

router.post('/:groupid/posts/tag-winners', function(req, res, next) {
    auctionController.tagAuctionPostWinners(req, res, next);
});

module.exports = router;
