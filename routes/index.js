var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Buy-Sell Clerk' });
});

router.get('/help', function(req, res, next) {
	res.render('help', { title: 'Help' });
});

router.get('/privacy_policy', function(req, res, next) {
	res.render('privacy_policy', { title: 'Privacy Policy' });
});

module.exports = router;
