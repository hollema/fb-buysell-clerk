const logger = require('../logger');

var express = require('express');
var router = express.Router();

var fbGraphService = require('../services/fbGraphService');

router.get('/', function(req, res, next) {
    if (req.session) {
        logger.info(`Destroying session ${req.sessionID}.`);
        req.session.destroy(function() {
            res.redirect('/');
        });
    }
});

module.exports = router;
