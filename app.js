const logger = require('./logger');

var express = require('express');
var session = require('express-session');
var MemcachedStore = require('connect-memcached')(session);
var passport = require('passport');
require('./auth/passport');

var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var login = require('./routes/login');
var logout = require('./routes/logout');
var dashboard = require('./routes/dashboard');
var sale = require('./routes/sale');
var auction = require('./routes/auction');

var app = express();

// app locals
app.locals.moment = require('moment');

// view engine setup
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

const sessionConfig = { 
    resave: false, 
    saveUninitialized: false, 
    secret: '60e630c3-c49b-4ae8-94f2-a775d5db2797', 
    signed: true
};

// In production use the App Engine Memcache instance to store session data,
// otherwise fallback to the default MemoryStore in development.
if (process.env.MEMCACHE_URL) { 
    logger.info('Using Memcached session store');
    sessionConfig.store = new MemcachedStore({
        hosts: [process.env.MEMCACHE_URL]
    });
}
app.use(session(sessionConfig));

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());

// As with any middleware it is quintessential to call next()
// if the user is authenticated
var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/')
};

app.use('/', index);
app.use('/login', login);
app.use('/logout', logout);
app.use('/dashboard', isAuthenticated, dashboard);
app.use('/sale', isAuthenticated, sale);
app.use('/auction', isAuthenticated, auction);
app.get('*', function(req, res, next) {
    res.redirect('/');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
