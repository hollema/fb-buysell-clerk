#!bin/bash

# run this script to set env vars for heroku

heroku config:set NODE_ENV=${NODE_ENV}

client_id=$(jq '.facebook.clientID' config/${NODE_ENV}.json -r)
heroku config:set CLIENT_ID=$client_id

client_secret=$(jq '.facebook.clientSecret' config/${NODE_ENV}.json -r)
heroku config:set CLIENT_SECRET=$client_secret

redirect_uri=$(jq '.redirect_uri' config/${NODE_ENV}.json -r)
heroku config:set REDIRECT_URI=$redirect_uri

log_level=$(jq '.log_level' config/${NODE_ENV}.json -r)
heroku config:set LOG_LEVEL=$log_level

debug=$(jq '.debug' config/${NODE_ENV}.json -r)
heroku config:set DEBUG=$debug

has_memcache_url=$(jq '. | has("memcache_url")' config/${NODE_ENV}.json)
if [ $has_memcache_url == 'true' ]
then
    memcache_url=$(jq '.memcache_url' config/${NODE_ENV}.json -r)
    heroku config:set MEMCACHE_URL=$memcache_url
fi
