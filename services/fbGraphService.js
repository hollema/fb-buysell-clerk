const logger = require('../logger');
var debug = require('debug')('clerk:fbGraphService');

var FB = require('fb');
var request = require('request-promise');
var crypto = require('crypto-extra');

var postFields = ' \
    type, \
    icon, \
    message, \
    application, \
    status_type, \
    link, \
    place, \
    permalink_url, \
    created_time, \
    updated_time \
';

module.exports = {

    getPageAccessToken: function(pageId, access_token, appsecret_proof) {
        logger.debug(`Getting page access token for page: ${pageId}.`);
        return FB.api(pageId, 'get', { fields: 'access_token', access_token: access_token, appsecret_proof: appsecret_proof });
    },

    destroyPermissions: function(userId, access_token, appsecret_proof) {
        logger.debug(`Destroying permissions for user ${userId}.`);
        return FB.api(userId + '/permissions', 'delete', { access_token: access_token, appsecret_proof: appsecret_proof });
    },

    getGroup: function(groupId, access_token, appsecret_proof) {
        logger.debug(`Getting group ${groupId}.`);
        return FB.api(groupId, 'get', { fields: 'name, icon', access_token: access_token, appsecret_proof: appsecret_proof });
    },

    getGroupPosts: function(groupId, limit, access_token, appsecret_proof) {
        logger.debug(`Getting group posts for group ${groupId} and limit ${limit}.`);
        return module.exports.getGroupPostsLimitOffset(groupId, limit, 0, access_token, appsecret_proof);
    },

    getGroupPostsSinceUntil: function(groupId, since, until, limit, access_token, appsecret_proof) { 
        logger.debug(`Getting group posts for group ${groupId}, since ${since}, until ${until}, limit ${limit}.`);
        return FB.api(groupId + '/feed', 'get', { 
            fields: postFields,
            since: since, until: until, limit: limit,
            access_token: access_token, appsecret_proof: appsecret_proof
        });
    },

    getGroupPostsLimitOffset: function(groupId, limit, offset, access_token, appsecret_proof) {
        logger.debug(`Getting group posts for group ${groupId}, limit ${limit}, offset ${offset}.`);
        return FB.api(groupId + '/feed', 'get', { 
            fields: postFields,
            limit: limit, offset: offset,
            access_token: access_token, appsecret_proof: appsecret_proof
        });
    },

    getGroupPostsWithComments: function(groupId, limit, access_token, appsecret_proof) {
        return FB.api(groupId + '/feed', 'get', { fields: 'permalink_url, message, created_time, updated_time, comments.order(reverse_chronological)', access_token: access_token, appsecret_proof: appsecret_proof, limit: limit });
    },

    goToUrl: function(url, appsecret_proof) {
        url = `${url}&appsecret_proof=${appsecret_proof}`;
        return request({ uri: url, json: true });
    },

    deletePosts: function(ids, access_token, appsecret_proof) {
        logger.debug(`Deleting posts with ids ${ids}.`);
        var deletes = [];

        ids.forEach(function(id) {
            debug(`post to delete: ${id}`);
            var delete_req = { method: 'delete', relative_url: id};
            deletes.push(delete_req);
        });

        return module.exports.doBatchPost(deletes, access_token, appsecret_proof);
    },

    getGroupsForUser: function(userId, limit, access_token, appsecret_proof) {
        return FB.api(userId + '/groups', 'get', { fields: 'name, icon, description, privacy', access_token: access_token, appsecret_proof: appsecret_proof, limit: limit });
    },

    getTaggableFriends: function(access_token, appsecret_proof) {
        return FB.api('/me/taggable_friends', 'get', { access_token: access_token, appsecret_proof: appsecret_proof });
    },

    getUserProfile: function(access_token, appsecret_proof) {
        return FB.api('me', 'get', { fields: 'name, email, about, picture, cover', access_token: access_token, appsecret_proof: appsecret_proof });
    },

    publishPost: function(groupId, message, tags, access_token, appsecret_proof) {
        return FB.api(groupId + '/feed', 'post', { message: message, tags: tags, access_token: access_token, appsecret_proof: appsecret_proof });
    },

    publishGroupPosts: function(groupId, messages, access_token, appsecret_proof) {
        var posts = [];
        messages.forEach(function(message) {
            debug(`message to publish: ${message}`);
            var body = 'message=' + message;
            var publish_req = { method: 'post', body: body, relative_url: groupId + '/feed'};
            posts.push(publish_req);
        });

        return module.exports.doBatchPost(posts, access_token, appsecret_proof);
    },

    /* only available for page access tokens, therefore not groups */
    publishComment: function(postId, message, access_token, appsecret_proof) {
        return FB.api(postId + '/comments', 'post', { message: message, access_token: access_token, appsecret_proof: appsecret_proof });
    },

    doBatchPost: function(batch, access_token, appsecret_proof) {
        return FB.api('', 'post', {
            batch: batch, 
            access_token: access_token, appsecret_proof: appsecret_proof });
    }
};
