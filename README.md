# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?
Hosts source code for Buy-Sell Clerk.

## How to set up for development
```npm install```

### Configuration
Get local.json config file from project administrator and add to /config

```
export NODE_ENV=local
source set-local-env.sh
```

### How to run the application
```npm run monitor```

### How to run tests
There are two robots that can create seed posts: Console and Web.
The Console robot uses selenium to automate interaction with the Facebook application.  The Web robot makes calls against the Facebook Graph API.

#### How to run Web Robot
```
cd tests/seeder
npm install
npm start
```
navigate in your browser to http://localhost:3001

### Dependencies
* node v7.7.4

---
## Deployment instructions
* Development is hosted at heroku.com
* Production is hosted at Google Cloud.

### Contribution guidelines

* Writing tests
* Code review
* Other guidelines

### Who do I talk to?

* Repo owner or admin
* Other community or team contact