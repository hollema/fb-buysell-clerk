$(function() {
    $("#discussion-posts-grid").bootgrid(postsGridConfig);
})
    .on('loaded.rs.jquery.bootgrid', function(e, newSelectedRows) {
        refreshDeleteButton("#discussion-posts-grid");
    })
    .on('selected.rs.jquery.bootgrid', function(e, newSelectedRows) {
        refreshDeleteButton("#discussion-posts-grid");
    })
    .on('deselected.rs.jquery.bootgrid', function(e, newSelectedRows) {
        refreshDeleteButton("#discussion-posts-grid");
    });
