$(function() {
    $("#auction-grid").bootgrid({
        converters: {
            "datetime": {
                from: function(value) { return moment(value).format('MM-DD-YYYY hh:mm:ss') },
                to: function(value) { return value; }
            },
        },

        formatters: {
            "currency_dollars": function(column, row) { 
                var value = row[column.id];
                if (isNaN(value)) {
                    return 'FREE';
                }
                return '$' + value;
            },

            "currency_dollars_cents": function(column, row) { 
                var value = row[column.id];
                return value.toLocaleString('en-US', {
                    style: 'currency',
                    currency: 'USD'
                });
            },

            "fb": function(column, row) {
                var link = row[column.id];
                if (link) {
                    return '<a href="' + link + '" target="_blank"><img src="/images/facebook_icon.svg"></a>';
                }
                else {
                    return '<span>-</span>';
                }
            },

            "status": function(column, row) {
                var value = row[column.id];
                var mappings = {
                    0: 'PASS',
                    1: 'INFO',
                    2: 'WARN',
                    3: 'FAIL'
                };
                return mappings[value];
            }
        },

        selection: true,
        multiSelect: true,
        rowCount: [10, 25, 50]
    });
});

function confirmTagWinnersForSelectedPosts() {
    var selectedRows = getSelectedRows();
    if (!selectedRows.length) {
        console.log("No posts selected to tag.");
        return;
    }

    $('#tagWinnersConfirmation').find('.modal-body').text('Tag winners for ' + selectedRows.length + ' selected posts?')
    $('#tagWinnersConfirmation').modal('show');
}

function tagWinnersForSelectedPosts(groupId) {
    var selectedRows = getSelectedRows();
    if (!selectedRows.length) {
        console.log("No posts selected to tag.");
        return;
    }

    var data = { "posts" : selectedRows };
    console.log("Tagging winners for these posts: ", JSON.stringify(data));

    $.ajax({
        url : `/auction/${groupId}/posts/tag-winners`,
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "text",
        success: function(response, status) {
            console.log("Data: " + response + "\nStatus: " + status);
            $('#tagWinnersCompleted').find('.modal-body').text(response)
            $('#tagWinnersCompleted').modal('show');

            //if (status === "success") {
            //  $('#msg-alert').addClass('alert-info');
            //  $('#msg-alert').removeClass('alert-danger');
            //}
            //else {
            //  $('#msg-alert').removeClass('alert-info');
            //  $('#msg-alert').addClass('alert-danger');
            //}
            //$('#msg-alert').text(response);
        },
        error: function(xhr, status, error) {
            console.log("Data: " + error + "\nStatus: " + status);
            $('#tagWinnersCompleted').find('.modal-body').text(error)
            $('#tagWinnersCompleted').modal('show');
        }
    });
}

function getSelectedRows() {
    var selectedRowIds = $("#auction-grid").bootgrid('getSelectedRows');
    var selectedRows = $("#auction-grid").bootgrid('getCurrentRows').filter(function(row) {
        return selectedRowIds.includes(row.id);
    });
    return selectedRows;
}
