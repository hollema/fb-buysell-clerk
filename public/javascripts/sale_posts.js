$(function() {
    $("#sale-posts-grid").bootgrid(postsGridConfig);
})
    .on('loaded.rs.jquery.bootgrid', function(e, newSelectedRows) {
        refreshDeleteButton("#sale-posts-grid");
    })
    .on('selected.rs.jquery.bootgrid', function(e, newSelectedRows) {
        refreshDeleteButton("#sale-posts-grid");
    })
    .on('deselected.rs.jquery.bootgrid', function(e, newSelectedRows) {
        refreshDeleteButton("#sale-posts-grid");
    });
