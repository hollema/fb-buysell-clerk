postsGridConfig = {
    converters: {
        "datetime": {
            from: function(value) { return moment(value); },
            to: function(value) { return value.format('lll'); }
        },

        "JSON": {
            from: function(value) { 
                if (value) {
                    return JSON.parse(value); 
                }
                return value;
            },
            to: function(value) { return value; }
        },
    },

    formatters: {
        "currency_dollars": function(column, row) { 
            var value = row[column.id];
            if (isNaN(value)) {
                return 'FREE';
            }
            return '$' + value;
        },

        "link": function(column, row) {
            var link = row[column.id];
            if (link) {
                return '<a href="' + link + '" target="_blank"><span class="glyphicon glyphicon-link"></span></a>';
            }
            else {
                return '<span>-</span>';
            }
        },

        "icon": function(column, row) {
            var icon = row[column.id];
            if (icon) {
                return '<img src="' + icon + '">';
            }
            else {
                return '<span class="glyphicon glyphicon-info-sign"></span>';
            }
        },

        "picture": function(column, row) {
            var picture = row[column.id];
            if (picture) {
                return '<img src="' + picture + '">';
            }
            else {
                return '<span>-</span>';
            }
        },

        "fb": function(column, row) {
            var link = row[column.id];
            if (link) {
                return '<a href="' + link + '" target="_blank"><img src="/images/facebook_icon.svg"></a>';
            }
            else {
                return '<span>-</span>';
            }
        },

        "place": function(column, row) {
            if (row.hasOwnProperty('place')) {
                return row.place.name;
            }
            else {
                return '<span>-</span>';
            }
        },

        "application": function(column, row) {
            if (row.hasOwnProperty('application')) {
                return row.application.app_name;
            }
            else {
                return '<span>-</span>';
            }
        },

        "NA": function(column, row) {
            var value = row[column.id];
            return value || '<span>-<span>';
        },
    },

    selection: true,
    multiSelect: true,
    rowCount: [50, 100, 250, 500],
    caseSensitive: false
}

function confirmDeleteSelectedPosts(postsGridId) {
    var selectedRows = getSelectedRows(postsGridId);
    if (!selectedRows.length) {
        console.log("No posts selected.");
        return;
    }

    $('#deletePostsConfirmation').find('.modal-body').text('Delete ' + selectedRows.length + ' posts?')
    $('#deletePostsConfirmation').modal('show');
}

function deleteSelectedPosts(groupId, postsGridId) {
    var selectedRows = getSelectedRows(postsGridId);
    if (!selectedRows.length) {
        console.log("No posts selected.");
        return;
    }

    var ids = selectedRows.map(r => r.id);
    var data = { "ids" : ids };
    console.log("Deleting these posts: ", JSON.stringify(data));

    $.ajax({
        url : `/sale/${groupId}/posts/delete`,
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "text",
        success: function(response, status) {
            console.log("Data: " + response + "\nStatus: " + status);
            $('#deletePostsCompleted').find('.modal-body').text(response)
            $('#deletePostsCompleted').modal('show');
        },
        error: function(xhr, status, error) {
            console.log("Data: " + error + "\nStatus: " + status);
            $('#deletePostsCompleted').find('.modal-body').text(error)
            $('#deletePostsCompleted').modal('show');
        }
    });
}

function getSelectedRows(postsGridId) {
    var selectedRowIds = $(postsGridId).bootgrid('getSelectedRows');
    var selectedRows = $(postsGridId).bootgrid('getCurrentRows').filter(function(row) {
        return selectedRowIds.includes(row.id);
    });
    return selectedRows;
}

function refreshDeleteButton(postsGridId) {
    var disabled = !getSelectedRows(postsGridId).length;
    $('#btn-delete').attr('disabled', disabled);
}
