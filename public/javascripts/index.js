$(function ($){
    $(document).ajaxStop(function(){
        $("#loader").hide();
        $(document).unmask();
    });
    $(document).ajaxStart(function(){
        $(document).mask();
        $("#loader").show();
    });    
});    

function reloadPosts(groupId) {
    var from_date;
    var to_date;
    var data = {};
    if ($('#date_from').length && $('#date_to').length) {
        from_date = moment($('#date_from').datepicker('getDate')).unix();
        to_date = moment($('#date_to').datepicker('getDate')).unix();
	console.log('from date: ' + from_date);
	console.log('to date: ' + to_date);

	data.since = from_date;
	data.until = to_date;
    }

    $.get(`/sale/${groupId}/reload-posts`, data).done(function(res) {
        document.open();
        document.write(res);
        document.close();
    }).fail(function(error) {
        alert(error.statusText + ": " + error.responseText);
        $(document).unmask();
    });
}
