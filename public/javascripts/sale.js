function confirmDeleteAllPosts() {
    var selected = getSelectedPostTypesForDelete();
    var message = `Delete ALL ${selected[0]}`
    if (selected[1]) {
        message += ` and ${selected[1]} `; 
    }
    message += ` posts?  This process CANNOT be undone.`; 

    $('#deleteAllPostsConfirmation').find('.modal-body').text(message);
    $('#deleteAllPostsConfirmation').modal('show');
}

function deleteAllPosts(groupId) {
    var selected = getSelectedPostTypesForDelete();
    var post_type = '';
    if (selected.length === 1 ) {
        post_type = selected[0];
    } else if (selected.length === 2) {
        post_type = 'all';
    }

    console.log("About to delete all posts for type: " + post_type);

    $.ajax({
        url : `/sale/${groupId}/posts/deleteAll`,
        type: "POST",
        data: JSON.stringify({"post_type": post_type}),
        contentType: "application/json; charset=utf-8",
        dataType: "text",
        success: function(response, status) {
            console.log("Data: " + response + "\nStatus: " + status);
            $('#deleteAllPostsCompleted').find('.modal-body').text(response)
            $('#deleteAllPostsCompleted').modal('show');
        },
        error: function(xhr, status, error) {
            console.log("Data: " + error + "\nStatus: " + status);
            $('#deleteAllPostsCompleted').find('.modal-body').text(error)
            $('#deleteAllPostsCompleted').modal('show');
        }
    });
}

function getSelectedPostTypesForDelete() {
    selected = [];
    $("input:checked").each(function(i, ob) {
        selected.push($(this).val());
    });

    return selected;
}

function refreshButtonsEnabled(salePosts, discussionPosts) {
    var disabled = !salePosts.length;
    if (disabled) {
        $('#btn-manage-sale-posts').attr('disabled', disabled);
        $('#btn-manage-sale-posts').removeAttr('href');
        $('#btn-tag-auction-winners').attr('disabled', disabled);
        $('#btn-tag-auction-winners').removeAttr('href');
    }

    disabled = !discussionPosts.length;
    if (disabled) {
        $('#btn-manage-discussion-posts').attr('disabled', disabled);
        $('#btn-manage-discussion-posts').removeAttr('href');
    }
}
