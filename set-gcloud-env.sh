#!bin/bash

# run this script to set env vars for gcloud

echo env_variables: >> app.yaml
echo ' ' NODE_ENV: ${NODE_ENV} >> app.yaml

echo ' ' CLIENT_ID: $(jq '.facebook.clientID' config/${NODE_ENV}.json -r) >> app.yaml
echo ' ' CLIENT_SECRET: $(jq '.facebook.clientSecret' config/${NODE_ENV}.json -r) >> app.yaml
echo ' ' REDIRECT_URI: $(jq '.redirect_uri' config/${NODE_ENV}.json -r) >> app.yaml
echo ' ' LOG_LEVEL: $(jq '.log_level' config/${NODE_ENV}.json -r) >> app.yaml

has_memcache_url=$(jq '. | has("memcache_url")' config/${NODE_ENV}.json)
if [ $has_memcache_url == 'true' ]
then
    memcache_url=$(jq '.memcache_url' config/${NODE_ENV}.json -r)
    echo ' ' MEMCACHE_URL: $memcache_url >> app.yaml
fi
