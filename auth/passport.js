const logger = require('../logger');

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;  
const crypto = require('crypto');

// serialize and deserialize
passport.serializeUser(function(user, done) {
    logger.debug(`Serializing user ${JSON.stringify(user)}.`);
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    logger.debug(`Deserializing user ${JSON.stringify(obj)}.`);
    done(null, obj);
});

passport.use(new FacebookStrategy({
    clientID: process.env.CLIENT_ID, 
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: process.env.REDIRECT_URI,
    profileFields: ['id', 'email', 'first_name', 'last_name'],
    enableProof: true
},
    function(accessToken, refreshToken, profile, done) {
        process.nextTick(function() {
            var user = {};
            user.id = profile.id;
            user.access_token = accessToken;
            user.appsecret_proof = generateHmac(accessToken, process.env.CLIENT_SECRET);
            user.name = profile.name.givenName + ' ' + profile.name.familyName;
            user.email = (profile.emails ? profile.emails[0].value : '').toLowerCase();
            logger.info(`Logging in user ${JSON.stringify(user)}.`);
            return done(null, user);
        });
    }
));

function generateHmac (data, secretKey) {
    var hmac = crypto.createHmac('sha256', secretKey).update(data).digest('hex');
    return hmac;
}
