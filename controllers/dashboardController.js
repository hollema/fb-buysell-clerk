var debug = require('debug')('clerk:dashboardController');
var BaseController = require('./baseController');
var fbGraphService = require('../services/fbGraphService');

const GROUPS_FETCH_LIMIT = 1000;

class DashboardController extends BaseController {

    getGroupsForUser(req, res, next) {
        fbGraphService.getUserProfile(req.user.access_token, req.user.appsecret_proof).then(function(user) {
            fbGraphService.getGroupsForUser(user.id, GROUPS_FETCH_LIMIT, req.user.access_token, req.user.appsecret_proof).then(function(groups_res) {
                var groups = groups_res.data;
                debug('Number groups fetched: ' + groups.length);
                res.render('dashboard', { title: 'Dashboard', user: user, groups: groups });
            }).catch(function(error) {
                BaseController.handleError(error);
            });
        }).catch(function(error) {
            BaseController.handleError(error);
        });
    }
}

module.exports = DashboardController;
