const logger = require('../logger');
var debug = require('debug')('clerk:baseController');

var fbGraphService = require('../services/fbGraphService');
var Promise = require('promise');

const DEFAULT_PAGE_SIZE = 50;

class BaseController {

    getGroup(req, res, next) {
        var groupId = req.params.groupid;
        logger.info(`Getting group ${groupId}.`);
        fbGraphService.getGroup(groupId, req.user.access_token, req.user.appsecret_proof)
            .then(function(group_res) {
                var group = group_res;

                if (!req.session.groups) {
                    logger.info('Session has no groups. Initialize session groups.');
                    req.session.groups = {};
                }

                if (!req.session.groups[groupId]) {
                    logger.info('Initializing group session data: ' +  groupId);

                    req.session.groups[groupId] = {
                        group : group,
                        salePosts : [],
                        discussionPosts: [],
                        postsLoaded: false,
                        since: Date.now()/1000 - (86400 * 30), // last 30 days
                        until: Date.now()/1000 + 86400 // tomorrow
                    };
                    BaseController.renderManageSaleView(res, req.session.groups[groupId]);
                } else {
                    logger.debug('Reading group data from session.');
                    var groupData = req.session.groups[groupId];
                    BaseController.renderManageSaleView(res, groupData);
                }
            }).catch(function(error) {
                BaseController.handleError(res, error);
            });
    }

    static getGroupPostsByDateRange(req, res, group, since, until) {
        var posts = [];

        return BaseController.getPostsByDateRange(group.id, since, until, posts, req.user.access_token, req.user.appsecret_proof)
        .then(function(result) {
            return BaseController.createGroupData(result, group, since, until); 
        }).catch(function(error) {
            BaseController.handleError(res, error);
        });
    }

    // BE CAREFUL - possible resource exhaustion, storing all posts in memory for group.
    static getAllGroupPosts(req, res, group, since, until) {
        var posts = [];
        var offset = 0;

        return BaseController.getAllPosts(group.id, offset, posts, req.user.access_token, req.user.appsecret_proof)
         .then(function(result) {
             return BaseController.createGroupData(result, group, since, until); 
         }).catch(function(error) {
             BaseController.handleError(res, error);
         });
    }

    static createGroupData(posts, group, since, until) {
        var salePosts = [];
        var discussionPosts = [];
        if (posts.length) {
            salePosts = BaseController.extractSalePosts(posts);
            logger.info(salePosts.length + ' total sale posts extracted.');
            discussionPosts = BaseController.extractDiscussionPosts(posts);
            logger.info(discussionPosts.length + ' total discussion posts extracted.');
        }

        var groupData = {
            group: group,
            salePosts: salePosts,
            discussionPosts: discussionPosts,
            since: since,
            until: until,
            postsLoaded: true
        };
        return groupData;
    }

    static renderManageSaleView(res, groupData) {
        logger.debug('rendering since : ' + groupData.since);
        logger.debug('rendering until : ' + groupData.until);

        var viewData = {
            title: 'Manage sale', 
            group: groupData.group,
            salePosts: groupData.salePosts,
            discussionPosts: groupData.discussionPosts,
            since: groupData.since,
            until: groupData.until,
            postsLoaded: groupData.postsLoaded
        };
        res.render('manage_sale', viewData);
    }

    static getPostsByDateRange(groupId, since, until, posts, access_token, appsecret_proof) {
        return new Promise(function(resolve, reject) {
            fbGraphService.getGroupPostsSinceUntil(groupId, since, until, DEFAULT_PAGE_SIZE, access_token, appsecret_proof).then(function(feed_res) {
                var postsBatch = feed_res.data;

                if (postsBatch.length && feed_res.paging && feed_res.paging.next) {
		    logger.debug(postsBatch.length + ' posts in current batch.');
		    posts = posts.concat(postsBatch);
                    resolve(BaseController.getPostsRecursive(feed_res.paging.next, posts, appsecret_proof));
                } else {
                    logger.debug('No more posts found.  Total size: ' + posts.length);
                    resolve(posts);
		}
            }).catch(function(error) {
                logger.error(error);
                reject(error);
            });
        });
    }

    static getPostsRecursive(nextPage, posts, appsecret_proof) { 
	return new Promise (function(resolve, reject) {
            fbGraphService.goToUrl(nextPage, appsecret_proof).then(function(feed_res) {
                var postsBatch = feed_res.data;

                if (postsBatch.length && (postsBatch.length + posts.length <= BaseController.MAX_FETCH_SIZE)) {
		    logger.debug(postsBatch.length + ' posts in current batch.');
		    posts = posts.concat(postsBatch);
                    BaseController.getPostsRecursive(feed_res.paging.next, posts, appsecret_proof).then(function(result) {
                        resolve(result);
		    });
                } else {
                    logger.debug('No more posts found.  Total size: ' + posts.length);
                    resolve(posts);
		}
            });
	});
    }

    static getAllPosts(groupId, offset, posts, access_token, appsecret_proof) {
        return new Promise(function(resolve, reject) {
            fbGraphService.getGroupPostsLimitOffset(groupId, BaseController.MAX_FETCH_SIZE, offset, access_token, appsecret_proof).then(function(feed_res) {
                var postsBatch = feed_res.data;
                if (postsBatch.length) {
                    logger.debug(postsBatch.length + ' posts in current batch.');
                    posts = posts.concat(postsBatch);
                    offset += BaseController.MAX_FETCH_SIZE;
                    BaseController.getAllPosts(groupId, offset, posts, access_token, appsecret_proof).then(function(result) {
                        resolve(result);
                    });
                } else {
                    logger.debug('No more posts found.  Total size: ' + posts.length);
                    resolve(posts);
                }
            }).catch(function(error) {
                logger.error(error);
                reject(error);
            });
        });
    }

    static extractSalePosts(posts) {
        var formattedSalePosts = [];
        var filteredPosts = filterSalePostsFromPosts(posts);
        if (filteredPosts) {
            formattedSalePosts = filteredPosts.map(formatSalePostData);
        }
        return formattedSalePosts;
    }

    static extractDiscussionPosts(posts) {
        var salePosts = BaseController.extractSalePosts(posts);
        var sale_post_ids = salePosts.map(function(sale_post) {
            return sale_post.id;
        });
        return posts.filter(function(post) {
            return !sale_post_ids.includes(post.id);
        });
    }

    static handleError(res, error) {
        logger.error(JSON.stringify(error));
        if (error.response && error.response.error && error.response.error.code) {
            if (error.response.error.code === 'ETIMEDOUT') {
                logger.error('request timeout');
            }
        }
        if (error.response && error.response.error) {
            logger.error(error.response.error);
        }
        if (error.message) {
            logger.error(error.message);
            res.status(500).send(error.message);
            return;
        }
        res.status(500).send(error);
    }

    static initializePaging(session, pagingKey, groupId) {
        if (!('paging'in session)) {
            session.paging = {};
        } 
        if (!(pagingKey in session.paging)) {
            session.paging[pagingKey] = {};
        } 
        if (!(groupId in session.paging[pagingKey])) {
            session.paging[pagingKey][groupId] = {};
        }
    }

    static cachePagingLimitOffset(session, pagingKey, groupId, limit, direction) {
        var limit = parseInt(limit);

        if (direction === 'reset') {
            session.paging[pagingKey][groupId].offset = 0; 
        }
        else if (direction === 'previous') {
            session.paging[pagingKey][groupId].offset = Math.max(session.paging[pagingKey][groupId].offset - limit, 0);
        }
        else if (direction === 'next') {
            session.paging[pagingKey][groupId].offset += limit;
        }

        session.paging[pagingKey][groupId].limit = limit;

        logger.info("Paging offset %s: ", session.paging[pagingKey][groupId].offset);
    }
}

BaseController.MAX_FETCH_SIZE = 1000;

function formatSalePostData(post) {

    post.title = post.message.split('\n')[0];
    post.description = post.message.split('\n')[3];

    var costAndLocation = post.message.split('\n')[1];

    var costMatch = /^FREE/;
    if (costMatch.test(costAndLocation)) {
        post.cost = 'FREE';
    } else {

        costMatch = /^\$(\d{0,3})/; // up to $999 only
        if (costMatch.test(costAndLocation)) {
            post.cost = costAndLocation.match(costMatch)[1];
        } else {
            post.cost = NaN;
        }}

    var locationMatch = / - (\d{5}$)/; // zip code
    if (locationMatch.test(costAndLocation)) {
        post.location = costAndLocation.match(locationMatch)[1];
    } else {
        var locationMatch = / - ([a-zA-Z\s]+, [a-zA-Z\s]+$)/; // City, State or City, ST
        if (locationMatch.test(costAndLocation)) {
            post.location = costAndLocation.match(locationMatch)[1];
        }
        else {
            post.location = "Unknown";
        }
    }

    return post;
}

function filterSalePostsFromPosts(posts) {
    return posts.filter(function (post) {
        return post.permalink_url && post.permalink_url.includes('sale_post_id'); 
    });
}

module.exports = BaseController;
