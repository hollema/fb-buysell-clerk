const logger = require('../logger');
var debug = require('debug')('clerk:auctionController');

var fbGraphService = require('../services/fbGraphService');
var Promise = require('promise');
var SaleController = require('./saleController');
var BaseController = require('./baseController');

const DEFAULT_PAGE_SIZE = 50;
const BATCH_SIZE = 50;
const PAGING_KEY = 'auction_posts';

class AuctionController {

    getAuctionPosts(req, res, next) {
        var groupId = req.params.groupid;
        var limit = req.query.limit || DEFAULT_PAGE_SIZE;
        renderPagedPosts(req, res, groupId, fbGraphService.getGroupPostsWithComments(groupId, limit, req.user.access_token, req.user.appsecret_proof));
    }

    tagAuctionPostWinners(req, res, next) {
        var msg = "No posts found in tag winners request.";
        if (!req.body || !req.body.posts) {
            BaseController.handleError(res, msg);
            return;
        }

        var posts = req.body.posts || [];
        if (!posts.length) {
            BaseController.handleError(res, msg);
            return;
        }


        logger.info("Tagging winners for posts: %j", posts);

        var tags = [];
        var data = "AUCTION WINNERS";

        data += posts.map(function(post) {
            tags.push(post['highest-bidder-id']);
            return `\n\n\n${post['highest-bidder']} won ${post['title']} for $${post['highest-bid']}\n\t(${post['link']})`;
        });

        logger.debug("Tagging data: " + data);

        var groupId = req.params.groupid;
        fbGraphService.publishPost(groupId, data, tags, req.user.access_token, req.user.appsecret_proof).then(function(publish_post_res) {

            var msg ='';
            if (publish_post_res) {
                msg = 'Winners tagged successfully.';
            }
            else {
                msg = 'Tagging winners failed';
            }
            res.send(msg);
        }).catch(function(error) {
            var msg = `Tag winners failed. \n${error.message}`;
            logger.error(msg);
            BaseController.handleError(res, error);
        });
    }
}

function renderPagedPosts(req, res, groupId, getPostsPromise) {
    //BaseController.initializePaging(req.session, PAGING_KEY, groupId);

    fbGraphService.getGroup(groupId, req.user.access_token, req.user.appsecret_proof).then(function(group_res) {
        var group = group_res;
        getPostsPromise.then(function(feed_res) {
            //BaseController.cachePaging(req.session, PAGING_KEY, groupId, feed_res.paging);

            var posts = SaleController.extractSalePosts(feed_res.data);
            var postsWithBids = posts.map(function(post) {
                post.bids = extractBids(post);
                return post;
            }).filter(function(post) {
                return post.bids.length;
            });

            postsWithBids.forEach(function(post) {
                var post = runQCforPredictedBid(post);
            });

            var viewData = {
                title: 'Auction', 
                group: group, 
                posts: postsWithBids
            };
            res.render(PAGING_KEY, viewData);
        }).catch(function(error) {
            BaseController.handleError(res, error);
        });
    }).catch(function(error) {
        BaseController.handleError(res, error);
    });
}

function extractBids(post) {
    if (!post.comments) {
        return [];
    }

    var comments = post.comments.data;
    var resemblesBid = /([0-9]?\.?[0-9][0-9]?)/;

    var bids = comments.filter(function(comment) {
        var isBid = resemblesBid.test(comment.message);
        return isBid;
    }).map(function(bid) {
        bid.amount = bid.message.match(resemblesBid)[0];
        return bid;
    }).sort(function(a, b) {
        return b.amount - a.amount;
    });

    return bids;
}

function runQCforPredictedBid(post) {
    var postCost = post.cost;
    if (isNaN(post.cost)) {
        debug.info("is nan");
        postCost = 1;
    }
    if ((post.bids[0].amount / postCost) > 3) {
        post.status= 2;
    }
    if ((post.bids[0].amount / postCost) > 5) {
        post.status= 3;
    }
    return post;
}

module.exports = AuctionController;
