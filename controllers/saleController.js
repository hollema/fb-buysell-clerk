const logger = require('../logger');
var debug = require('debug')('clerk:saleController');

var fbGraphService = require('../services/fbGraphService');
var Promise = require('promise');
var BaseController = require('./baseController');

const BATCH_DELETE_SIZE = 50;

const SALE_POSTS_DATA = {
    pagingKey: 'salePosts',
    title: 'Manage sale posts',
    view: 'sale_posts'
}

const DISCUSSION_POSTS_DATA = {
    pagingKey: 'discussionPosts',
    title: 'Manage discussion posts',
    view: 'discussion_posts'
}

class SaleController extends BaseController {

    getDiscussionPosts(req, res, next) {
        logger.info('Getting discussion posts.');
        var groupId = req.params.groupid;
        SaleController.renderPosts(req, res, next, groupId, DISCUSSION_POSTS_DATA);
    }

    getSalePosts(req, res, next) {
        logger.info('Getting sale posts.');
        var groupId = req.params.groupid;
        SaleController.renderPosts(req, res, next, groupId, SALE_POSTS_DATA);
    }

    reloadPosts(req, res, next) {
        logger.info('Reloading posts.');
        var groupId = req.params.groupid;

        logger.info('Getting group posts for group: ' + groupId);
        var group = req.session.groups[groupId].group;

        var since = req.query.since ? req.query.since : req.session.groups[groupId].since;
        logger.debug('reload posts since: ' + since);
        var until = req.query.until ? req.query.until : req.session.groups[groupId].until;
        logger.debug('reload posts until: ' + until);

        BaseController.getGroupPostsByDateRange(req, res, group, since, until).then(function(groupData) {
            logger.info('Setting session posts data for group: ' +  groupId);
            req.session.groups[groupId] = groupData;

            BaseController.renderManageSaleView(res, groupData);
        });
    }

    deleteSalePosts(req, res, next) {
        logger.info('Deleting sale posts.');

        var msg = 'No posts found in delete request.';
        if (!req.body || !req.body.ids) {
            BaseController.handleError(res, msg);
            return;
        }

        var ids = req.body.ids || [];
        if (!ids.length) {
            BaseController.handleError(res, msg);
            return;
        }

        doDelete(ids, req.user.access_token, req.user.appsecret_proof).then(function(results) {
            var msg = `${results.pass} posts deleted successfully.`;
            if (results.fail) {
                msg += `\n${results.fail} posts failed to delete.`;
            }
            res.send(msg);
        });
    }

    deleteAllPosts(req, res, next) {
        logger.info('Deleting all posts.');

        var msg = 'No posts type in delete request.';
        if (!req.body || !req.body.post_type) {
            BaseController.handleError(res, msg);
            return;
        }

        var postType = req.body.post_type;
        logger.info(`Deleting all ${postType} posts.`);

        var groupId = req.params.groupid;
        var results = { pass: 0, fail: 0, total: 0 };

        deletePostsRecursive(groupId, results, postType, req.user.access_token, req.user.appsecret_proof).then(function() {
            var msg = `${results.pass} posts deleted successfully.`;
            if (results.fail) {
                msg += `\n${results.fail} posts failed to delete.`;
            }
            logger.info(msg);
            res.send(msg);
        }).catch(function(error) {
            logger.error('Delete posts recursive failed: ' + error);
            BaseController.handleError(res, error);
        });
    }

    static renderPosts(req, res, next, groupId, postsData) {
        var viewData = {
            title: postsData.title,
            group: req.session.groups[groupId].group, 
            posts: req.session.groups[groupId][postsData.pagingKey]
        };
        res.render(postsData.view, viewData);
    }
}

function deletePostsRecursive(groupId, results, postType, access_token, appsecret_proof) {

    return fbGraphService.getGroupPosts(groupId, BaseController.MAX_FETCH_SIZE, access_token, appsecret_proof)
        .then(function(feed_res) {
            if (feed_res.data.length) {
                var posts = filterPostsByType(postType, feed_res.data);
                logger.info('Deleting ' + posts.length + ' posts.');
                var ids = posts.map(function(post) {
                    return post.id;
                });
                return doDelete(ids, access_token, appsecret_proof);
            } else {
                return Promise.resolve();
            }
        })
        .then(function(batch_results) {
            if ((batch_results.fail === batch_results.total) || !batch_results) {
                logger.error('Entire batch failed to delete.');
                return Promise.resolve();
            }
            results.pass += batch_results.pass;
            results.fail += batch_results.fail;
            return deletePostsRecursive(groupId, results, postType, access_token, appsecret_proof);
        });
}

function doDelete(ids, access_token, appsecret_proof) {

    
    var n = Math.floor(ids.length/BATCH_DELETE_SIZE);
    logger.debug(n + ' batches of ' + BATCH_DELETE_SIZE);
    
    var promises = [];
    var i = 0;

    while (i <= n) {
      logger.debug(`i: ${i}`);
      var batch = ids.slice(i*BATCH_DELETE_SIZE, (i+1)*BATCH_DELETE_SIZE-1);
      logger.debug(`Deleting batch of ${batch.length} items.`);
      logger.debug('do delete batch: ' + batch);
      var promise = fbGraphService.deletePosts(batch, access_token, appsecret_proof);
      promises.push(promise);
      i++;
    }

    return Promise.all(promises).then(function(resolved_promises) {
        var results = { pass: 0, fail: 0, total: 0 };
        resolved_promises.forEach(function(delete_responses) {
          delete_responses.forEach(function(delete_res) {
              results.total++;
              logger.debug(`Delete response:  ${JSON.stringify(delete_res)}`);
              logger.debug(`Delete response code: ${delete_res.code}, body: ${delete_res.body}`);
              if (delete_res.code === 200) {
                  logger.debug('Item deleted successfully.');
                  results.pass++;
              }
              else {
                  logger.error('Item failed to delete: ' + delete_res.body);
                  results.fail++;
              }
          });
        });
        logger.debug('Results: ' + JSON.stringify(results));
        return results;
    });
}

function filterPostsByType(postType, posts) {
    if (postType === 'sale') {
        return SaleController.extractSalePosts(posts);
    } else if (postType === 'discussion') {
        return SaleController.extractDiscussionPosts(posts);
    } else {
        return posts;
    }
    return [];
}

module.exports = SaleController;
